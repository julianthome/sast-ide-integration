# using git submodules
We use [git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) to manage project collaborations.

## checkout submodules at their currently referenced commits
When this repository is first checked out, the submodule directories must be populated, e.g.
```sh
git submodule update --init --recursive
```

## update submodules to the HEAD of their set branches
To pull the latest from each submodule's set branch, run the following:
```sh
git submodule update --init --recursive --remote --rebase
```
Note, the `--rebase` flag is only necessary if a submodule is tracking a branch.  Without `--rebase`, any local commits will be ignored and the submodule will go into detached HEAD state at the remote branch HEAD.

If a submodule has changed, `git status` will show "new commits" for the submodule.  To stage changes to a submodule, `git add` its path, e.g.
```sh
git submodule add sast-rules
git submodule add semgrep
git submodule add gitlab-vscode-extension
```

## set submodule branches for collaborating branches of component projects
Assuming that each component project has a branch `sast-ide/my-feature`, create a branch in this repo to use them together, e.g.
```sh
git switch -c my-feature
git submodule set-branch -b sast-ide/my-feature sast-rules
git submodule set-branch -b sast-ide/my-feature semgrep
git submodule set-branch -b sast-ide/my-feature gitlab-vscode-extension
# update to the HEAD of each remote submodule branch
git submodule update --init --recursive --remote --rebase
# stage changes to the submodules configuration
git add .gitmodules
git add sast-rules
git add semgrep
git add gitlab-vscode-extension
```

## troubleshooting
- Read the [git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) chapter in the Pro Git book.
- Work out rebasing issues within each submodule directory then re-reun `git submodules update`.
- If a submodule is in detached HEAD state, you can `git switch` to its set branch.
