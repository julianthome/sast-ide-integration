# SAST IDE integration
This project brings together the components of GitLab's "SAST in the IDE" solution for development, demos, integration testing, and benchmarking.

## debugging the extension on a mac with vscode
After [initial setup](#initial-setup), the [scanner service](#start-the-scanner-service-locally) and [extension](#debug-the-extension-from-visual-studio-code) can be debugged locally in tandem.

### initial setup
Run
```sh
./scripts/setup.sh
```

The [setup script](scripts/setup.sh):
1. [checks out submodules](./docs/submodules.md#checkout-submodules-at-their-currently-referenced-commits)
2. uses `asdf` to install `go`, `python`, and `ruby`
3. creates a Python virtual environment and installs `semgrep` with `pip`
4. runs `asdf install` in the `gitlab-vscode-extension` submodule and runs `npm install`

This covers the prerequisites of each component:

- The `semgrep` analyzer service is written in `go` and uses the `python` wrapper for `semgrep` on the `PATH`.
- The `sast-rules` project distribution of `semgrep` rules is [built](scripts/build-rules.sh) with a `ruby` script.
- The `gitlab-vscode-extension` project manages its requirements in its own `.tool-versions` file.

### start the scanner service locally
```sh
./scripts/start-scanner-service.sh
```
ctrl-c to stop it.

### debug the extension from Visual Studio Code
```sh
code  ./gitlab-vscode-extension
```
Note: The security scan feature is disabled by default. To enable it, press shift-cmd-p, type "Preferences: Open User Settings (JSON)", and hit enter. Insert the key
```json
    "gitlab.featureFlags.remoteSecurityScans": true,
```
and save.

From the `gitlab-vscode-extension` project,
1. Press F5 to start debugging the extension. A new window with title beginning `[Extension Development Host]` will open.  
2. In the extension development host window, navigate to a project with some vulnerabilities.
3. To trigger a scan, either save a file or press shift-cmd-p, type "GitLab: Run Security Scan", and hit enter.
4. To see the `log` message from the extension, open the output window - press shift-cmd-u and select "GitLab Workflow" from the drop down.
