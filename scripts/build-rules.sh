#!/bin/zsh
SCRIPTS="${0:A:h}"
BASE="${SCRIPTS:A:h}"

cd "$BASE/sast-rules"

path=("$(go env GOPATH)/bin" "$path[@]")

set -e

rm -rf ./dist ./sast-rules.zip

./ci/run_deploy.rb 0.0.0

./ci/unique_ids.rb

rm -rf "$BASE/rules"
mv dist "$BASE/rules"