#!/bin/zsh

SCRIPTS="${0:A:h}"
BASE="${SCRIPTS:A:h}"

. "$BASE/.venv/bin/activate"

if [ ! -d "$BASE/rules" ]; then
    "$SCRIPTS/build-rules.sh"
fi

cd "$BASE/semgrep"
go run . -- serve -c "$BASE/rules"
