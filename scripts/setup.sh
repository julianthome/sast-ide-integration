#!/bin/zsh
SCRIPTS="${0:A:h}"
BASE="${SCRIPTS:A:h}"

cd "$BASE"

git submodule update --init --recursive

SCANNER_VERSION=$(sed -n -e 's/^ARG SCANNER_VERSION=//p' "$BASE/semgrep/Dockerfile")

asdf install

if [ ! -d "$BASE/.venv" ]; then
    python -mvenv .venv
fi

. "$BASE/.venv/bin/activate"

pip3 install semgrep==$SCANNER_VERSION

cd "$BASE/gitlab-vscode-extension"
asdf install
npm install
